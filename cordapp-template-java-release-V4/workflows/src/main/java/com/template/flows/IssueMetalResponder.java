package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import net.corda.core.flows.*;
import net.corda.core.transactions.SignedTransaction;

// ******************
// * Responder flow *
// ******************
@InitiatedBy(IssueMetal.class)
public class IssueMetalResponder extends FlowLogic<SignedTransaction> {
    private FlowSession counterpartySession;

    public IssueMetalResponder(FlowSession counterpartySession) {
        this.counterpartySession = counterpartySession;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        // Responder flow logic goes here.
        System.out.println("Received Precious metal");
        return subFlow(new ReceiveFinalityFlow(counterpartySession));
    }
}
