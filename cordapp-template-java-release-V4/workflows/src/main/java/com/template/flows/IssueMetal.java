package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.contracts.MetalContract;
import com.template.states.MetalState;
import net.corda.core.contracts.Command;
import net.corda.core.flows.*;
import net.corda.core.identity.Party;
import net.corda.core.transactions.SignedTransaction;
import net.corda.core.transactions.TransactionBuilder;
import net.corda.core.utilities.ProgressTracker;

// ******************
// * Initiator flow *
// ******************
@InitiatingFlow
@StartableByRPC
public class IssueMetal extends FlowLogic<SignedTransaction> {
    private final ProgressTracker.Step RETRIEVING_NOTARY = new ProgressTracker.Step("Retrieving the notary");
    private final ProgressTracker.Step GENERATING_TRANSACTION = new ProgressTracker.Step("Generating transaction");
    private final ProgressTracker.Step SIGNING_TRANSACTION = new ProgressTracker.Step("Signing transaciton with our private key");
    private final ProgressTracker.Step COUNTER_PARTY_SESSION = new ProgressTracker.Step("sending flow to the counter party");
    private final ProgressTracker.Step FINALISING_TRANSACTION = new ProgressTracker.Step("Obtaining notary signature and recording the transaction");

    private final String metalName;
    private final int weight;
    private final Party owner;
    private final ProgressTracker progressTracker = new ProgressTracker(RETRIEVING_NOTARY,
            GENERATING_TRANSACTION, SIGNING_TRANSACTION, COUNTER_PARTY_SESSION, FINALISING_TRANSACTION);

    public IssueMetal(String metalName, int weight, Party owner) {
        this.metalName = metalName;
        this.weight = weight;
        this.owner = owner;
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public SignedTransaction call() throws FlowException {
        // Initiator flow logic goes here.
        //retrieve notary identity
        progressTracker.setCurrentStep(RETRIEVING_NOTARY);
        final Party notary = getServiceHub().getNetworkMapCache().getNotaryIdentities().get(0);
        //Create transaction components
        MetalState metalState = new MetalState(metalName, weight, getOurIdentity(), owner);
        Command command = new Command(new MetalContract.Issue(), getOurIdentity().getOwningKey());
        //generating transactions
        progressTracker.setCurrentStep(GENERATING_TRANSACTION);
        TransactionBuilder txnBuilder = new TransactionBuilder(notary).addOutputState(metalState, MetalContract.ID).addCommand(command);

        //Sign the Txn
        progressTracker.setCurrentStep(SIGNING_TRANSACTION);
        final SignedTransaction signedTransaction = getServiceHub().signInitialTransaction(txnBuilder);

        //Create session with counter party
        progressTracker.setCurrentStep(COUNTER_PARTY_SESSION);
        //initiate flow to the target
        final FlowSession flowSession = initiateFlow(owner);

        //Finalising the transaction
        progressTracker.setCurrentStep(FINALISING_TRANSACTION);
        return subFlow(new FinalityFlow(signedTransaction, flowSession));
    }
}
