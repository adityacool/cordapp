package com.template.flows;

import co.paralleluniverse.fibers.Suspendable;
import com.template.states.MetalState;
import net.corda.core.contracts.StateAndRef;
import net.corda.core.flows.FlowException;
import net.corda.core.flows.FlowLogic;
import net.corda.core.flows.InitiatingFlow;
import net.corda.core.flows.StartableByRPC;
import net.corda.core.node.services.Vault;
import net.corda.core.node.services.vault.QueryCriteria;
import net.corda.core.utilities.ProgressTracker;

import java.util.List;

// ******************
// * Initiator flow *
// ******************
@InitiatingFlow
@StartableByRPC
public class SearchVault extends FlowLogic<Void> {
    private final ProgressTracker progressTracker = new ProgressTracker();

    public SearchVault() {
    }

    @Override
    public ProgressTracker getProgressTracker() {
        return progressTracker;
    }

    @Suspendable
    @Override
    public Void call() throws FlowException {
        System.out.println("starting SearchVault");
        searchForAllStates();
        return null;
    }

    private void searchForAllStates()  {
        System.out.println("Inside searchForAllStates");
        System.out.println("Querying consumed states");
        QueryCriteria consumedQueryCriteria = new QueryCriteria.VaultQueryCriteria(Vault.StateStatus.CONSUMED);
        final List<StateAndRef<MetalState>> cosumedMetalStatePage = getServiceHub().getVaultService().queryBy(MetalState.class, consumedQueryCriteria).getStates();
        if (cosumedMetalStatePage.isEmpty()) {
            System.out.println("No unconsumed metal states found");
        } else {
            System.out.println("Total consumed metal states \n" + cosumedMetalStatePage.size());
        }
        cosumedMetalStatePage.forEach(c -> {
            System.out.println("Metal Name - " + c.getState().getData().getMetalName());
            System.out.println("weight - " + c.getState().getData().getWeight());
            System.out.println("Issuer - " + c.getState().getData().getIssuer());
            System.out.println("Owner - " + c.getState().getData().getOwner());
        });
        System.out.println("Querying unconsumed states");
        QueryCriteria unConsumedQueryCriteria = new QueryCriteria.VaultQueryCriteria(Vault.StateStatus.UNCONSUMED);
        final List<StateAndRef<MetalState>> unConsumedMetalStatePage = getServiceHub().getVaultService().queryBy(MetalState.class, unConsumedQueryCriteria).getStates();
        if (unConsumedMetalStatePage.isEmpty()) {
            System.out.println("No unconsumed metal states found");
        } else {
            System.out.println("Total unconsumed metal states \n" + unConsumedMetalStatePage.size());
        }
        unConsumedMetalStatePage.forEach(c -> {
            System.out.println("Metal Name - " + c.getState().getData().getMetalName());
            System.out.println("weight - " + c.getState().getData().getWeight());
            System.out.println("Issuer - " + c.getState().getData().getIssuer());
            System.out.println("Owner - " + c.getState().getData().getOwner());
        });


    }
}
